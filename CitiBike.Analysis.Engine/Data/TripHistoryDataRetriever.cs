﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using Microsoft.VisualBasic.FileIO;

namespace CitiBike.Analysis.Engine.Data
{
    public interface ITripHistoryDataRetriever
    {
        void DownloadTripHistoryData(int year, int month, string destinationFileName);

        IEnumerable<TripHistoryInfo> ReadTripHistoryData(string sourceFileName, IReadOnlyDictionary<int, GeoCoordinate> stationCoordinates);
    }

    internal sealed class TripHistoryDataRetriever : ITripHistoryDataRetriever
    {
        private static readonly string[] _delimiters = { "," };

        public void DownloadTripHistoryData(int year, int month, string destinationFileName)
        {
            WebRequest request = WebRequest.CreateHttp($"https://s3.amazonaws.com/tripdata/{year:0000}{month:00}-citibike-tripdata.zip");
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            if (request.Proxy != null)
            {
                request.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
            }
            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return;
                    }
                    using (Stream fileStream = File.Create(destinationFileName))
                    {
                        stream.CopyTo(fileStream);
                    }
                }
            }
        }

        public IEnumerable<TripHistoryInfo> ReadTripHistoryData(string sourceFileName, IReadOnlyDictionary<int, GeoCoordinate> stationCoordinates)
        {
            using (FileStream fileStream = File.OpenRead(sourceFileName))
            {
                using (ZipArchive archive = new ZipArchive(fileStream, ZipArchiveMode.Read))
                {
                    ZipArchiveEntry entry = archive.Entries.First();
                    using (Stream entryStream = entry.Open())
                    {
                        using (TextFieldParser parser = new TextFieldParser(entryStream) { Delimiters = TripHistoryDataRetriever._delimiters })
                        {
                            int startTimeIndex = -1, stopTimeIndex = -1, startStationIdIndex = -1, endStationIdIndex = -1, bikeIdIndex = -1;
                            int index = -1;
                            string[] headers = parser.ReadFields();
                            if (headers == null)
                            {
                                yield break;
                            }
                            foreach (string header in headers)
                            {
                                index++;
                                switch (header.ToLowerInvariant())
                                {
                                    case "starttime":
                                    case "start time":
                                        startTimeIndex = index;
                                        break;
                                    case "stoptime":
                                    case "stop time":
                                        stopTimeIndex = index;
                                        break;
                                    case "start station id":
                                    case "start stationid":
                                    case "startstationid":
                                        startStationIdIndex = index;
                                        break;
                                    case "end station id":
                                    case "end stationid":
                                    case "endstationid":
                                        endStationIdIndex = index;
                                        break;
                                    case "bikeid":
                                    case "bike id":
                                        bikeIdIndex = index;
                                        break;
                                    default:
                                        continue;
                                }
                                if (startTimeIndex != -1 && stopTimeIndex != -1 && startStationIdIndex != -1 && endStationIdIndex != -1 && bikeIdIndex != -1)
                                {
                                    break;
                                }
                            }
                            if (startTimeIndex == -1 || stopTimeIndex == -1 || startStationIdIndex == -1 || endStationIdIndex == -1 || bikeIdIndex == -1)
                            {
                                yield break;
                            }
                            string[] fields;
                            while ((fields = parser.ReadFields()) != null && fields.Length == headers.Length)
                            {
                                int startStationId = int.Parse(fields[startStationIdIndex]);
                                int endStationId = int.Parse(fields[endStationIdIndex]);
                                yield return new TripHistoryInfo(
                                    int.Parse(fields[bikeIdIndex]),
                                    startStationId,
                                    endStationId,
                                    DateTime.Parse(fields[startTimeIndex]),
                                    DateTime.Parse(fields[stopTimeIndex]),
                                    TripHistoryDataRetriever.GetDistance(startStationId,endStationId,stationCoordinates)
                                );
                            }
                        }
                    }
                }
            }
        }

        private static double GetDistance(int startStationId, int endStationId, IReadOnlyDictionary<int, GeoCoordinate> stationCoordinates)
        {
            GeoCoordinate startCoordinates, endCoordinates;
            if (startStationId == endStationId ||
                !stationCoordinates.TryGetValue(startStationId, out startCoordinates) ||
                !stationCoordinates.TryGetValue(endStationId, out endCoordinates))
            {
                // Stations have been removed from the system on rare instances.
                return 0d;
            }
            const double metersPerMile = 1609.344;
            return startCoordinates.GetDistanceTo(endCoordinates) / metersPerMile;
        }
    }
}