﻿using System;

namespace CitiBike.Analysis.Engine.Data
{
    public sealed class TripHistoryInfo
    {
        public TripHistoryInfo(int bikeId, int startStationId, int endStationId, DateTime startTime, DateTime stopTime, double miles)
        {
            this.BikeId = bikeId;
            this.StartStationId = startStationId;
            this.EndStationId = endStationId;
            this.StartTime = startTime;
            this.StopTime = stopTime;
            this.Miles = miles;
        }

        public int BikeId
        {
            get;
        }

        public int EndStationId
        {
            get;
        }

        public double Miles
        {
            get;
        }

        public int StartStationId
        {
            get;
        }

        public DateTime StartTime
        {
            get;
        }

        public DateTime StopTime
        {
            get;
        }

        public int StopTimeOffset => this.StopTime.GetOffset();
    }
}