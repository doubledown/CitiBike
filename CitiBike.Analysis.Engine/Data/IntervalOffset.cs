﻿using System;

namespace CitiBike.Analysis.Engine.Data
{
    public static class IntervalOffset
    {
        public static int GetOffset(this DateTime date)
        {
            if (date == DateTime.MinValue)
            {
                return 0;
            }
            return (int)Math.Floor((date - DataDates.StartTime).TotalHours);
        }
    }
}