﻿using System;

namespace CitiBike.Analysis.Engine.Data
{
    public static class DataDates
    {
        public const int Year = 2015;
        public static readonly DateTime StartTime = new DateTime(2015, 1, 1);
        public static readonly DateTime EndTime = new DateTime(2015, 12, 31, 23, 59, 59, 999);
    }

    public static class DateTimeMethods
    {
        public static int GetDateId(this DateTime dateTime)
        {
            return dateTime.Year * 1000 + dateTime.Month * 100 + dateTime.Day;
        }
    }
}