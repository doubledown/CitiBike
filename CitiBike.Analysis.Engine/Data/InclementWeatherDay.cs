﻿namespace CitiBike.Analysis.Engine.Data
{
    public enum WeatherType
    {
        None = 0,
        Rain = 1,
        Snow = 2
    }

    public sealed class InclementWeatherDay
    {
        public InclementWeatherDay(int dateId, WeatherType weatherType)
        {
            this.DateId = dateId;
            this.WeatherType = weatherType;
        }

        public int DateId
        {
            get;
        }

        public WeatherType WeatherType
        {
            get;
        }
    }
}