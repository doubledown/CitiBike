using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace CitiBike.Analysis.Engine.Data
{
    public sealed class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container
                .Register(Component.For<IDataManager, IConnectionFactory>().ImplementedBy<DataManager>())
                .Register(Component.For<ITripHistoryDataRetriever>().ImplementedBy<TripHistoryDataRetriever>())
                .Register(Component.For<IInclementWeatherDataRetriever>().ImplementedBy<InclementWeatherDataRetriever>())
                .Register(Component.For<IStationDataRetriever>().ImplementedBy<StationDataRetriever>());
        }
    }
}