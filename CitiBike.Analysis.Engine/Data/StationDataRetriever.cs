﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Newtonsoft.Json;

namespace CitiBike.Analysis.Engine.Data
{
    public interface IStationDataRetriever
    {
        IEnumerable<Station> GetStationInformation();
    }

    internal sealed class StationDataRetriever : IStationDataRetriever
    {
        private readonly JsonSerializer _serializer;

        public StationDataRetriever(JsonSerializer serializer)
        {
            this._serializer = serializer;
        }

        public IEnumerable<Station> GetStationInformation()
        {
            WebRequest request = WebRequest.CreateHttp("https://gbfs.citibikenyc.com/gbfs/en/station_information.json");
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            if (request.Proxy != null)
            {
                request.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
            }
            ICollection<StationInfo> stationInfos;
            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return null;
                    }
                    using (TextReader textReader = new StreamReader(stream))
                    {
                        using (JsonReader jsonReader = new JsonTextReader(textReader))
                        {
                            StationResponseModel model = new StationResponseModel();
                            this._serializer.Populate(jsonReader, model);
                            stationInfos = model.Data.Stations;
                        }
                    }
                }
            }
            return stationInfos.Select(info => new Station
            {
                Id = info.Id,
                Name = info.Name,
                Latitude = info.Latitude,
                Longitude = info.Longitude,
                RegionId = info.RegionId,
                Capacity = info.Capacity
            });
        }

        private sealed class StationInfo
        {
            [JsonProperty("capacity")]
            public int Capacity
            {
                get;
                set;
            }

            [JsonProperty("station_id")]
            public int Id
            {
                get;
                set;
            }

            [JsonProperty("lat")]
            public double Latitude
            {
                get;
                set;
            }

            [JsonProperty("lon")]
            public double Longitude
            {
                get;
                set;
            }

            [JsonProperty("name")]
            public string Name
            {
                get;
                set;
            }

            [JsonProperty("region_id")]
            public int RegionId
            {
                get;
                set;
            } = Regions.NewYork;
        }

        private sealed class StationResponseModel
        {
            [JsonProperty("data")]
            public StationResponseModelData Data
            {
                get;
            } = new StationResponseModelData();
        }

        private sealed class StationResponseModelData
        {
            [JsonProperty("stations")]
            public List<StationInfo> Stations
            {
                get;
            } = new List<StationInfo>();
        }
    }
}