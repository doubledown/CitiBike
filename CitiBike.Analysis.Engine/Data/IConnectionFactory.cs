﻿using System.Data.SqlClient;

namespace CitiBike.Analysis.Engine.Data
{
    public interface IConnectionFactory
    {
        SqlConnection CreateConnection(bool mars = false);
    }
}