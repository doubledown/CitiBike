namespace CitiBike.Analysis.Engine.Data
{
    public sealed class Station
    {
        public int Capacity
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int RegionId
        {
            get;
            set;
        }
    }
}