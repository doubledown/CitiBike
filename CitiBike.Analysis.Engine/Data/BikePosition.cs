﻿using System;

namespace CitiBike.Analysis.Engine.Data
{
    public sealed class BikePosition
    {
        public int BikeId
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public double Miles
        {
            get;
            set;
        }

        public int MonthId
        {
            get;
            set;
        }

        public int Rides
        {
            get;
            set;
        }

        public int StationId
        {
            get;
            set;
        }

        public int StopTimeOffset
        {
            get;
            set;
        }
    }
}