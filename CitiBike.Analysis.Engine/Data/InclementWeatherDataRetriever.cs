﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading;

namespace CitiBike.Analysis.Engine.Data
{
    public interface IInclementWeatherDataRetriever
    {
        IEnumerable<InclementWeatherDay> GetInclementWeatherData(int year);
    }

    public interface IInclementWeatherDay
    {
        int DateId
        {
            get;
        }

        WeatherType WeatherType
        {
            get;
        }
    }

    internal sealed class InclementWeatherDataRetriever : IInclementWeatherDataRetriever
    {
        private const string CentralParkStationId = "USW00094728";
        private const string JfkStationId = "USW00094789";
        private const string PrecipitationIndicator = "PRCP";
        private const string SnowIndicator = "SNOW";

        public IEnumerable<InclementWeatherDay> GetInclementWeatherData(int year)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create($"ftp://ftp.ncdc.noaa.gov/pub/data/ghcn/daily/by_year/{year}.csv.gz");
            request.UseBinary = true;
            request.Timeout = request.ReadWriteTimeout = Timeout.Infinite;
            string tempFileName = Path.GetTempFileName();
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            // Should never happen.
                            yield break;
                        }
                        using (FileStream fileStream = File.OpenWrite(tempFileName))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                }
                WeatherType weatherType = 0;
                int dateId = 0;
                using (FileStream fileStream = File.OpenRead(tempFileName))
                {
                    using (GZipStream gzip = new GZipStream(fileStream, CompressionMode.Decompress))
                    {
                        using (StreamReader reader = new StreamReader(gzip))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                int firstCommaIndex = line.IndexOf(',');
                                if (firstCommaIndex != 11)
                                {
                                    continue;
                                }
                                string stationId = line.Substring(0, 11);
                                if (stationId != InclementWeatherDataRetriever.CentralParkStationId && stationId != InclementWeatherDataRetriever.JfkStationId)
                                {
                                    continue;
                                }
                                int lineDateId = int.Parse(line.Substring(12, 8));
                                if (dateId < lineDateId)
                                {
                                    if (weatherType != 0)
                                    {
                                        yield return new InclementWeatherDay(dateId, weatherType);
                                    }
                                    dateId = lineDateId;
                                    weatherType = 0;
                                }
                                int commaIndex = line.IndexOf(',', 21);
                                string field = line.Substring(21, commaIndex - 21);
                                if (field != InclementWeatherDataRetriever.SnowIndicator && field != InclementWeatherDataRetriever.PrecipitationIndicator)
                                {
                                    continue;
                                }
                                int valueCommaIndex = line.IndexOf(',', commaIndex + 1);
                                string value = line.Substring(commaIndex + 1, valueCommaIndex - commaIndex - 1);
                                if (weatherType != WeatherType.Snow && value != "0")
                                {
                                    weatherType = field == InclementWeatherDataRetriever.SnowIndicator ? WeatherType.Snow : WeatherType.Rain;
                                }
                            }
                        }
                    }
                    if (dateId != 0 && weatherType != 0)
                    {
                        yield return new InclementWeatherDay(dateId, weatherType);
                    }
                }
            }
            finally
            {
                File.Delete(tempFileName);
            }
        }
    }
}