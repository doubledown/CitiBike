﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Device.Location;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CitiBike.Analysis.Engine.Data
{
    public interface IDataManager
    {
        void PrepareData();
    }

    internal sealed class DataManager : IDataManager, IConnectionFactory
    {
        public const string DatabaseName = "CitiBike";
        public const string LocalDbConnectionString = @"Server=(LocalDb)\MSSQLLocalDB;Integrated Security=SSPI";

        private readonly IInclementWeatherDataRetriever _inclementWeatherDataRetriever;
        private readonly IStationDataRetriever _stationDataRetriever;
        private readonly ITripHistoryDataRetriever _tripHistoryDataRetriever;

        public DataManager(IStationDataRetriever stationDataRetriever, IInclementWeatherDataRetriever inclementWeatherDataRetriever, ITripHistoryDataRetriever tripHistoryDataRetriever)
        {
            Console.Write("Checking that database exists...");
            using (SqlConnection connection = new SqlConnection(DataManager.LocalDbConnectionString))
            {
                connection.Open();
                bool exists;
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT CONVERT(BIT, CASE WHEN EXISTS (SELECT NULL FROM [sys].[databases] WHERE [Name] = @database) THEN 1 ELSE 0 END)";
                    command.Parameters.AddWithValue("@database", DataManager.DatabaseName);
                    exists = (bool)command.ExecuteScalar();
                }
                if (!exists)
                {
                    Console.WriteLine("Not Found");
                    Console.Write("Creating...");
                    this.CreateDatabase(connection);
                }
            }

            Console.WriteLine("Done");
            this._inclementWeatherDataRetriever = inclementWeatherDataRetriever;
            this._tripHistoryDataRetriever = tripHistoryDataRetriever;
            this._stationDataRetriever = stationDataRetriever;
        }

        SqlConnection IConnectionFactory.CreateConnection(bool mars)
        {
            return DataManager.CreateConnection(mars);
        }

        public void PrepareData()
        {
            Console.WriteLine("Preparing 2015 data...");
            IReadOnlyDictionary<int, GeoCoordinate> stationCoordinates = null;
            DataManager.WrapWithStopWatch(() => stationCoordinates = this.PrepareStationData());
            DataManager.WrapWithStopWatch(() => this.PrepareTripHistoryData(stationCoordinates));
            DataManager.WrapWithStopWatch(this.PrepareInclementWeatherData);
            Console.WriteLine("2015 data is ready!");
        }

        private static SqlConnection CreateConnection(bool mars = false)
        {
            StringBuilder builder = new StringBuilder(DataManager.LocalDbConnectionString).Append(";Database=").Append(DataManager.DatabaseName);
            if (mars)
            {
                builder.Append(";MultipleActiveResultSets=True");
            }
            return new SqlConnection(builder.ToString());
        }

        private static IEnumerable<Station> StreamStations(IEnumerable<Station> stations, IDictionary<int, GeoCoordinate> dictionary)
        {
            foreach (Station station in stations)
            {
                dictionary.Add(station.Id, new GeoCoordinate(station.Latitude, station.Longitude));
                yield return station;
            }
        }

        private static void WrapWithStopWatch(Action action)
        {
            Stopwatch watch = Stopwatch.StartNew();
            action.Invoke();
            watch.Stop();
            Console.WriteLine(" ({0:#,##0.###}s)", watch.Elapsed.TotalSeconds);
        }

        private void CreateDatabase(SqlConnection connection)
        {
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = $"CREATE DATABASE [{DataManager.DatabaseName}]";
                command.ExecuteNonQuery();
            }
            connection.ChangeDatabase(DataManager.DatabaseName);
            TableManager<BikePosition>.CreateTable(connection, bikePosition => bikePosition.BikeId, bikePosition => bikePosition.StopTimeOffset);
            TableManager<BikePosition>.CreateIndex(connection, bikePosition => bikePosition.BikeId);
            TableManager<BikePosition>.CreateIndex(connection, bikePosition => bikePosition.StationId);
            TableManager<BikePosition>.CreateIndex(connection, bikePosition => bikePosition.StopTimeOffset);
            TableManager<Station>.CreateTable(connection, station => station.Id);
            TableManager<InclementWeatherDay>.CreateTable(connection, inclementWeatherDay => inclementWeatherDay.DateId);
        }

        private void PrepareInclementWeatherData()
        {
            Console.Write("Preparing weather data...");
            using (SqlConnection connection = DataManager.CreateConnection())
            {
                connection.Open();
                string query = $"SELECT CONVERT(BIT, CASE WHEN EXISTS (SELECT NULL FROM [dbo].[{nameof(InclementWeatherDay)}] WHERE [{nameof(InclementWeatherDay.DateId)}] / 10000 = {DataDates.Year}) THEN 1 ELSE 0 END)";
                bool dataExists;
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    dataExists = (bool)command.ExecuteScalar();
                }
                if (!dataExists)
                {
                    TableManager<InclementWeatherDay>.CreateTable(connection);
                    TableManager<InclementWeatherDay>.BulkLoadToTable(connection, this._inclementWeatherDataRetriever.GetInclementWeatherData(DataDates.Year));
                    TableManager<InclementWeatherDay>.CopyFromTempTable(connection);
                }
            }
            Console.Write("Done");
        }

        private IReadOnlyDictionary<int, GeoCoordinate> PrepareStationData()
        {
            Console.Write("Preparing station data...");
            Dictionary<int, GeoCoordinate> dictionary = new Dictionary<int, GeoCoordinate>();
            using (SqlConnection connection = DataManager.CreateConnection())
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = $"SELECT [{nameof(Station.Id)}], [{nameof(Station.Latitude)}], [{nameof(Station.Longitude)}] FROM [dbo].[{nameof(Station)}]";
                    using (SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        while (dataReader.Read())
                        {
                            dictionary.Add(dataReader.GetInt32(0), new GeoCoordinate(dataReader.GetDouble(1), dataReader.GetDouble(2)));
                        }
                    }
                }
                if (dictionary.Count == 0)
                {
                    TableManager<Station>.CreateTable(connection);
                    TableManager<Station>.BulkLoadToTable(connection, DataManager.StreamStations(this._stationDataRetriever.GetStationInformation(), dictionary));
                    TableManager<Station>.CopyFromTempTable(connection);
                }
            }
            Console.Write("Done");
            return dictionary;
        }

        private void PrepareTripHistoryData(IReadOnlyDictionary<int, GeoCoordinate> stationCoordinates)
        {
            Console.Write("Checking trip history data....");
            using (SqlConnection connection = DataManager.CreateConnection())
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = $"SELECT CONVERT(BIT, CASE WHEN EXISTS (SELECT NULL FROM [dbo].[{nameof(BikePosition)}]) THEN 1 ELSE 0 END)";
                    if ((bool)command.ExecuteScalar())
                    {
                        Console.Write("Done");
                        return;
                    }
                    Console.WriteLine("Incomplete!");
                }
            }
            Console.Write("Downloading trip history data...");
            const int numMonths = 12;
            Parallel.For(
                0,
                numMonths,
                new ParallelOptions { MaxDegreeOfParallelism = 5 },
                months =>
                {
                    DateTime date = DataDates.StartTime.AddMonths(months);
                    this._tripHistoryDataRetriever.DownloadTripHistoryData(date.Year, date.Month, Path.Combine(Path.GetTempPath(), "trips" + months + ".data"));
                }
            );
            Console.WriteLine("Done");
            Dictionary<int, BikePosition> dictionary = new Dictionary<int, BikePosition>();
            using (SqlConnection connection = DataManager.CreateConnection())
            {
                connection.Open();
                TableManager<BikePosition>.CreateTable(connection);
                for (int months = 0; months < numMonths; months++)
                {
                    string fileName = Path.Combine(Path.GetTempPath(), "trips" + months + ".data");
                    DateTime date = DataDates.StartTime.AddMonths(months);
                    Console.Write("Processing bike position data for {0:MMM yyyy}...", date);
                    TableManager<BikePosition>.BulkLoadToTable(connection, this.StreamBikePositions(dictionary, fileName, stationCoordinates));
                    TableManager<BikePosition>.CopyFromTempTable(connection);
                    TableManager<BikePosition>.TruncateTempTable(connection);
                    Console.WriteLine("Done");
                }
                Console.Write("Writing final bike position data...");
                TableManager<BikePosition>.BulkLoadToTable(connection, dictionary.Values.Where(position => position.Date <= DataDates.EndTime));
                TableManager<BikePosition>.CopyFromTempTable(connection);
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "DELETE [dbo].[Station] WHERE [Id] NOT IN (SELECT DISTINCT [StationId] FROM [dbo].[BikePosition])";
                    command.CommandTimeout = 600;
                    command.ExecuteNonQuery();
                }
                Console.Write("Done");
            }
        }

        private IEnumerable<BikePosition> StreamBikePositions(IDictionary<int, BikePosition> dictionary, string fileName, IReadOnlyDictionary<int, GeoCoordinate> stationCoordinates)
        {
            try
            {
                foreach (TripHistoryInfo info in this._tripHistoryDataRetriever.ReadTripHistoryData(fileName, stationCoordinates))
                {
                    BikePosition position;
                    if (dictionary.TryGetValue(info.BikeId, out position))
                    {
                        if (position.StationId == info.EndStationId)
                        {
                            // Rides to the same station are ignored as we can't calculate distance.
                            continue;
                        }
                        if (info.StopTimeOffset <= position.StopTimeOffset)
                        {
                            position.Rides++;
                            position.Date = info.StopTime;
                            position.StationId = info.EndStationId;
                            position.Miles += info.Miles;
                            continue;
                        }
                        yield return position;
                    }
                    dictionary[info.BikeId] = new BikePosition
                    {
                        BikeId = info.BikeId,
                        Date = info.StopTime,
                        MonthId = info.StopTime.Year * 100 + info.StopTime.Month,
                        Rides = 1,
                        Miles = info.Miles,
                        StationId = info.EndStationId,
                        StopTimeOffset = info.StopTimeOffset
                    };
                }
            }
            finally
            {
                File.Delete(fileName);
            }
        }

        private static class TableManager<T>
        {
            private static PropertyInfo[] Properties
            {
                get;
            } = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);

            public static void BulkLoadToTable(SqlConnection connection, IEnumerable<T> collection, bool isTempTable = true)
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock, null))
                {
                    bulkCopy.DestinationTableName = string.Concat(isTempTable ? "#" : string.Empty, typeof(T).Name);
                    bulkCopy.EnableStreaming = true;
                    bulkCopy.BulkCopyTimeout = 600;
                    bulkCopy.BatchSize = 10000;
                    using (DataReader dataReader = new DataReader(collection))
                    {
                        bulkCopy.WriteToServer(dataReader);
                    }
                }
            }

            public static void CopyFromTempTable(SqlConnection connection)
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    string columnNames = string.Join(",", TableManager<T>.Properties.Select(property => string.Concat("[", property.Name, "]")));
                    command.CommandText = $"INSERT [dbo].[{typeof(T).Name}] ({columnNames}) SELECT {columnNames} FROM [#{typeof(T).Name}]";
                    command.CommandTimeout = 600;
                    command.ExecuteNonQuery();
                }
            }

            public static void CreateIndex<TProperty>(SqlConnection connection, Expression<Func<T, TProperty>> indexExpression)
            {
                string columnName = ((MemberExpression)indexExpression.Body).Member.Name;
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = string.Format("CREATE INDEX [IX_{1}] ON [{0}]([{1}])", typeof(T).Name, columnName);
                    command.ExecuteNonQuery();
                }
            }

            public static void CreateTable(SqlConnection connection, params Expression<Func<T, object>>[] keyColumnExpressions)
            {
                bool isTempTable = keyColumnExpressions == null || keyColumnExpressions.Length == 0;
                StringBuilder builder = new StringBuilder("CREATE TABLE [")
                    .Append(isTempTable ? "#" : "dbo].[")
                    .Append(typeof(T).Name)
                    .Append("](");
                for (int index = 0; index < Properties.Length; index++)
                {
                    if (index != 0)
                    {
                        builder.Append(',');
                    }
                    PropertyInfo property = Properties[index];
                    builder.Append('[').Append(property.Name).Append("] ");
                    switch (Type.GetTypeCode(property.PropertyType))
                    {
                        case TypeCode.Boolean:
                            builder.Append("BIT");
                            break;
                        case TypeCode.Int32:
                            builder.Append("INT");
                            break;
                        case TypeCode.Double:
                            builder.Append("FLOAT");
                            break;
                        case TypeCode.DateTime:
                            builder.Append("DATETIME");
                            break;
                        case TypeCode.String:
                            builder.Append("NVARCHAR(125)");
                            break;
                        default:
                            Debugger.Break();
                            break;
                    }
                    builder.Append(" NOT NULL");
                }
                if (!isTempTable)
                {
                    builder.Append(", CONSTRAINT [PK_")
                        .Append(typeof(T).Name)
                        .Append("] PRIMARY KEY (");
                    for (int index = 0; index < keyColumnExpressions.Length; index++)
                    {
                        if (index != 0)
                        {
                            builder.Append(',');
                        }
                        Expression expression = keyColumnExpressions[index].Body;
                        if (expression.NodeType == ExpressionType.Convert)
                        {
                            expression = ((UnaryExpression)expression).Operand;
                        }
                        if (expression.NodeType != ExpressionType.MemberAccess)
                        {
                            throw new ArgumentOutOfRangeException(nameof(keyColumnExpressions));
                        }
                        builder.Append('[').Append(((MemberExpression)expression).Member.Name).Append(']');
                    }
                    builder.Append(')');
                }
                builder.Append(')');
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = builder.ToString();
                    command.ExecuteNonQuery();
                }
            }

            public static void TruncateTempTable(SqlConnection connection)
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = $"TRUNCATE TABLE [#{typeof(T).Name}]";
                    command.CommandTimeout = 600;
                    command.ExecuteNonQuery();
                }
            }

            private sealed class DataReader : IDataReader
            {
                private static readonly Func<T, string, object> _getValueFunction = DataReader.CreateGetValueFunction();

                private readonly IEnumerator<T> _enumerator;

                public DataReader(IEnumerable<T> collection)
                {
                    this._enumerator = collection.GetEnumerator();
                }

                public int Depth => 0;

                public int FieldCount => TableManager<T>.Properties.Length;

                public bool IsClosed
                {
                    get;
                    private set;
                }

                public int RecordsAffected => 0;

                public object this[int i] => this[TableManager<T>.Properties[i].Name];

                public object this[string name] => DataReader._getValueFunction(this._enumerator.Current, name);

                public void Close()
                {
                    this.Dispose();
                }

                public void Dispose()
                {
                    this._enumerator.Dispose();
                    this.IsClosed = true;
                }

                public string GetDataTypeName(int i)
                {
                    return this.GetFieldType(i).Name;
                }

                public Type GetFieldType(int i)
                {
                    return TableManager<T>.Properties[i].PropertyType;
                }

                public string GetName(int i)
                {
                    return TableManager<T>.Properties[i].Name;
                }

                public int GetOrdinal(string name)
                {
                    return Array.FindIndex(TableManager<T>.Properties, property => property.Name == name);
                }

                DataTable IDataReader.GetSchemaTable()
                {
                    throw new NotImplementedException();
                }

                bool IDataRecord.GetBoolean(int i)
                {
                    throw new NotImplementedException();
                }

                byte IDataRecord.GetByte(int i)
                {
                    throw new NotImplementedException();
                }

                long IDataRecord.GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
                {
                    throw new NotImplementedException();
                }

                char IDataRecord.GetChar(int i)
                {
                    throw new NotImplementedException();
                }

                long IDataRecord.GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
                {
                    throw new NotImplementedException();
                }

                IDataReader IDataRecord.GetData(int i)
                {
                    throw new NotImplementedException();
                }

                DateTime IDataRecord.GetDateTime(int i)
                {
                    throw new NotImplementedException();
                }

                decimal IDataRecord.GetDecimal(int i)
                {
                    throw new NotImplementedException();
                }

                double IDataRecord.GetDouble(int i)
                {
                    throw new NotImplementedException();
                }

                float IDataRecord.GetFloat(int i)
                {
                    throw new NotImplementedException();
                }

                Guid IDataRecord.GetGuid(int i)
                {
                    throw new NotImplementedException();
                }

                short IDataRecord.GetInt16(int i)
                {
                    throw new NotImplementedException();
                }

                int IDataRecord.GetInt32(int i)
                {
                    throw new NotImplementedException();
                }

                long IDataRecord.GetInt64(int i)
                {
                    throw new NotImplementedException();
                }

                string IDataRecord.GetString(int i)
                {
                    throw new NotImplementedException();
                }

                object IDataRecord.GetValue(int i)
                {
                    return this[i];
                }

                int IDataRecord.GetValues(object[] values)
                {
                    throw new NotImplementedException();
                }

                public bool IsDBNull(int i)
                {
                    return this[i] == DBNull.Value;
                }

                public bool NextResult()
                {
                    return false;
                }

                public bool Read()
                {
                    return this._enumerator.MoveNext();
                }

                private static Func<T, string, object> CreateGetValueFunction()
                {
                    ParameterExpression itemParameter = Expression.Parameter(typeof(T), "item");
                    ParameterExpression columnNameParameter = Expression.Parameter(typeof(string), "columnName");
                    return Expression.Lambda<Func<T, string, object>>(
                        Expression.Switch(
                            typeof(object),
                            columnNameParameter,
                            Expression.Field(
                                null,
                                typeof(Convert),
                                nameof(Convert.DBNull)
                            ),
                            null,
                            Array.ConvertAll(
                                typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public),
                                property => Expression.SwitchCase(
                                    Expression.Coalesce(
                                        Expression.Convert(
                                            Expression.Property(
                                                itemParameter,
                                                property
                                            ),
                                            typeof(object)
                                        ),
                                        Expression.Field(
                                            null,
                                            typeof(Convert),
                                            nameof(Convert.DBNull)
                                        )
                                    ),
                                    Expression.Constant(
                                        property.Name
                                    )
                                )
                            )
                        ),
                        itemParameter,
                        columnNameParameter
                    ).Compile();
                }
            }
        }
    }
}