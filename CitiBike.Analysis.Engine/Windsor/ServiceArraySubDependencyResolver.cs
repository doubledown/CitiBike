﻿using System;
using Castle.Core;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;
using Castle.MicroKernel.Registration;

namespace CitiBike.Analysis.Engine.Windsor
{
    public sealed class ServiceArraySubDependencyResolver : ISubDependencyResolver
    {
        private readonly IKernel _kernel;
        private readonly Type _serviceType;

        public ServiceArraySubDependencyResolver(IKernel kernel, Type serviceType)
        {
            (this._kernel = kernel).Register(Classes.FromAssemblyContaining(serviceType).BasedOn(serviceType).WithServiceSelf());
            this._serviceType = serviceType;
        }

        public bool CanResolve(CreationContext context, ISubDependencyResolver contextHandlerResolver, ComponentModel model, DependencyModel dependency)
        {
            return dependency.TargetType == this._serviceType.MakeArrayType();
        }

        public object Resolve(CreationContext context, ISubDependencyResolver contextHandlerResolver, ComponentModel model, DependencyModel dependency)
        {
            IHandler[] handlers = this._kernel.GetAssignableHandlers(this._serviceType);
            Array result = Array.CreateInstance(this._serviceType, handlers.Length);
            for (int index = 0; index < handlers.Length; index++)
            {
                IHandler handler = handlers[index];
                result.SetValue(this._kernel.Resolve(handler.ComponentModel.Implementation), index);
            }
            return result;
        }
    }
}