﻿using System;
using System.Linq;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace CitiBike.Analysis.Engine.Properties
{
    public sealed class WindsorInstaller : IWindsorInstaller
    {
        void IWindsorInstaller.Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For(
                    from interfaceType in typeof(Settings).GetInterfaces()
                    where interfaceType.Namespace != null && interfaceType.Namespace.StartsWith(nameof(CitiBike), StringComparison.Ordinal)
                    select interfaceType
                ).Instance(Settings.Default)
            );
        }
    }
}
