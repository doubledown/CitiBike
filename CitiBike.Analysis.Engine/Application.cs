﻿using CitiBike.Analysis.Engine.Data;
using CitiBike.Analysis.Engine.Web;

namespace CitiBike.Analysis.Engine
{
    public interface IApplication
    {
        void Start();

        void Stop();
    }

    internal sealed class Application : IApplication
    {
        private readonly IDataManager _dataManager;
        private readonly IOwinService _owinService;

        public Application(IDataManager dataManager, IOwinService owinService)
        {
            this._dataManager = dataManager;
            this._owinService = owinService;
        }

        public void Start()
        {
            this._dataManager.PrepareData();
            this._owinService.Start();
        }

        public void Stop()
        {
            this._owinService.Stop();
        }
    }
}