﻿using System;
using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace CitiBike.Analysis.Engine
{
    public sealed class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container
                .Register(Component.For<IApplication>().ImplementedBy<Application>())
                .Register(Component.For<IContractResolver>().Instance(FilteredCamelCasePropertyNamesContractResolver.Instance))
                .Register(Component.For<JsonSerializer>().Instance(new JsonSerializer { ContractResolver = FilteredCamelCasePropertyNamesContractResolver.Instance }));
        }

        private sealed class FilteredCamelCasePropertyNamesContractResolver : DefaultContractResolver
        {
            public static readonly FilteredCamelCasePropertyNamesContractResolver Instance = new FilteredCamelCasePropertyNamesContractResolver();

            private static readonly JsonConverter _stringEnumConverter = new StringEnumConverter();

            private FilteredCamelCasePropertyNamesContractResolver()
            {
            }

            protected override JsonContract CreateContract(Type objectType)
            {
                JsonContract contract = base.CreateContract(objectType);
                if (FilteredCamelCasePropertyNamesContractResolver.CanApplyChanges(objectType) &&
                    objectType.IsValueType &&
                    (Nullable.GetUnderlyingType(objectType) ?? objectType).IsEnum)
                {
                    contract.Converter = FilteredCamelCasePropertyNamesContractResolver._stringEnumConverter;
                }
                return contract;
            }

            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                JsonProperty jsonProperty = base.CreateProperty(member, memberSerialization);
                if (FilteredCamelCasePropertyNamesContractResolver.CanApplyChanges(member.DeclaringType))
                {
                    jsonProperty.PropertyName = FilteredCamelCasePropertyNamesContractResolver.ConvertToCamelCase(jsonProperty.PropertyName);
                }
                return jsonProperty;
            }

            private static bool CanApplyChanges(Type type)
            {
                // SignalR specific objects are prefixed with Microsoft and they can not have their properties be camel cased.
                return type?.Namespace != null && !type.Assembly.IsDynamic && !type.Namespace.StartsWith(nameof(Microsoft), StringComparison.Ordinal);
            }

            private static string ConvertToCamelCase(string text)
            {
                if (text.Length == 0 || char.IsLower(text, 0))
                {
                    return text;
                }
                char[] characters = text.ToCharArray();
                characters[0] = char.ToLowerInvariant(characters[0]);
                return new string(characters);
            }
        }
    }
}