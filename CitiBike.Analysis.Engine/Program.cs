﻿using System;
using System.Reflection;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Topshelf;
using Topshelf.HostConfigurators;

namespace CitiBike.Analysis.Engine
{
    internal static class Program
    {
        private static void Main()
        {
            using (IWindsorContainer container = new WindsorContainer())
            {
                HostFactory.Run(Program.TopshelfConfiguration(container));
            }
        }

        private static Action<HostConfigurator> TopshelfConfiguration(IWindsorContainer container)
        {
            return hostConfig =>
            {
                container.Install(FromAssembly.This());
                hostConfig.Service<IApplication>(config =>
                {
                    config.ConstructUsing(container.Resolve<IApplication>);
                    config.WhenStarted(application => application.Start());
                    config.WhenStopped(application => application.Stop());
                });
                string serviceName = Assembly.GetExecutingAssembly().GetName().Name;
                hostConfig.SetServiceName(serviceName);
                AssemblyProductAttribute productAttribute = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyProductAttribute>();
                string displayName = string.IsNullOrEmpty(productAttribute?.Product) ? serviceName : productAttribute.Product;
                hostConfig.SetDisplayName(displayName);
                AssemblyDescriptionAttribute descriptionAttribute = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>();
                string description = string.IsNullOrEmpty(descriptionAttribute?.Description) ? displayName : descriptionAttribute.Description;
                hostConfig.SetDescription(description);
                hostConfig.RunAsLocalSystem();
                hostConfig.StartAutomaticallyDelayed();
            };
        }
    }
}