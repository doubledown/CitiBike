﻿namespace CitiBike.Analysis.Engine.Web
{
    public interface IWebSettings
    {
        bool IsWebPortWhitelisted
        {
            get;
        }

        int WebPort
        {
            get;
        }
    }
}