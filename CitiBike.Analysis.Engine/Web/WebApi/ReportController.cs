﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using CitiBike.Analysis.Engine.Reports;

namespace CitiBike.Analysis.Engine.Web.WebApi
{
    public sealed class ReportController : ApiController
    {
        private readonly IReadOnlyDictionary<string, IReport> _reports;

        public ReportController(IReport[] reports)
        {
            this._reports = reports.ToDictionary(report => report.Name);
        }

        public object GetData(string name, int year, int month, int day = 0)
        {
            IReport report;
            return !this._reports.TryGetValue(name, out report) ? null : report.GetData(year, month, day);
        }
    }
}