﻿using System;
using System.Diagnostics;
using System.Net;
using Microsoft.Owin.Hosting;

namespace CitiBike.Analysis.Engine.Web
{
    public interface IOwinService
    {
        void Start();

        void Stop();
    }

    internal sealed class OwinService : IOwinService, IDisposable
    {
        private readonly IOwinStartup[] _startupHandlers;
        private readonly string _url;
        private IDisposable _webApplication;

        public OwinService(IWebSettings webSettings, IOwinStartup[] startupHandlers)
        {
            // If you whitelist a port using netsh urlacl commands, you can skip the administrator check.  
            // Otherwise unelevated processes can only host on localhost. 
            this._url = $"http://{(webSettings.IsWebPortWhitelisted || UacProperties.IsProcessElevated ? "+" : "localhost")}:{webSettings.WebPort}/CitiBike/";
            this._startupHandlers = startupHandlers;
        }

        public void Dispose()
        {
            this.Stop();
        }

        void IOwinService.Start()
        {
            Console.Write("Starting web services...");
            this._webApplication = WebApp.Start(
                new StartOptions(this._url),
                app =>
                {
                    HttpListener listener = (HttpListener)app.Properties[typeof(HttpListener).FullName];
                    listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
                    Array.ForEach(this._startupHandlers, handler => handler.OnStartup(app));
                }
            );
            Console.WriteLine("Done");
            Console.WriteLine("Listening at {0}", this._url);
            if (Environment.UserInteractive)
            {
                Process.Start(this._url);
            }
        }

        void IOwinService.Stop()
        {
            this.Stop();
        }

        private void Stop()
        {
            if (this._webApplication == null)
            {
                return;
            }
            Console.Write("Stopping web application...");
            this._webApplication.Dispose();
            this._webApplication = null;
            Console.WriteLine("Done");
        }
    }
}