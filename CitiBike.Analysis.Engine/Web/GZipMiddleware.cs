﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Owin;

namespace CitiBike.Analysis.Engine.Web
{
    internal sealed class GZipMiddleware : OwinMiddleware
    {
        private const long CompressionThreshold = 0x2000;
        private const int CopyBufferLength = 0x10000;

        public GZipMiddleware(OwinMiddleware next)
            : base(next)
        {
        }

        public override async Task Invoke(IOwinContext context)
        {
            string[] acceptEncoding;
            if (context.Request.Headers.ContainsKey("Sec-WebSocket-Version") ||
                !context.Request.Headers.TryGetValue("Accept-Encoding", out acceptEncoding) ||
                Array.TrueForAll(acceptEncoding, encoding => encoding.IndexOf("gzip", StringComparison.Ordinal) == -1))
            {
                await this.Next.Invoke(context);
                return;
            }
            Stream outputStream = context.Response.Body;
            try
            {
                using (MemoryStream workingStream = new MemoryStream())
                {
                    context.Response.Body = workingStream;
                    await this.Next.Invoke(context);
                    if (workingStream.Length == 0)
                    {
                        return;
                    }
                    workingStream.Seek(0, SeekOrigin.Begin);
                    if (workingStream.Length <= GZipMiddleware.CompressionThreshold)
                    {
                        context.Response.ContentLength = workingStream.Length;
                        await workingStream.CopyToAsync(outputStream, (int)workingStream.Length, context.Request.CallCancelled);
                        return;
                    }
                    context.Response.Headers["Content-Encoding"] = "gzip";
                    if (String.Equals(context.Request.Protocol, "HTTP/1.1", StringComparison.Ordinal))
                    {
                        context.Response.Headers["Transfer-Encoding"] = "chunked";
                        await GZipMiddleware.CompressAsync(workingStream, outputStream, context.Request.CallCancelled);
                        return;
                    }
                    using (MemoryStream bufferStream = new MemoryStream())
                    {
                        await GZipMiddleware.CompressAsync(workingStream, bufferStream, context.Request.CallCancelled);
                        bufferStream.Seek(0, SeekOrigin.Begin);
                        context.Response.ContentLength = bufferStream.Length;
                        await bufferStream.CopyToAsync(outputStream, GZipMiddleware.CopyBufferLength, context.Request.CallCancelled);
                    }
                }
            }
            finally
            {
                context.Response.Body = outputStream;
            }
        }

        private static async Task CompressAsync(Stream sourceStream, Stream targetStream, CancellationToken cancellationToken)
        {
            using (GZipStream gzipStream = new GZipStream(targetStream, CompressionMode.Compress, true))
            {
                await sourceStream.CopyToAsync(gzipStream, GZipMiddleware.CopyBufferLength, cancellationToken);
            }
        }
    }
}