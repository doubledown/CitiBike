﻿using Owin;

namespace CitiBike.Analysis.Engine.Web
{
    internal interface IOwinStartup
    {
        void OnStartup(IAppBuilder app);
    }
}