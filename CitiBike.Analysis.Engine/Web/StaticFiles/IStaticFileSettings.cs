﻿namespace CitiBike.Analysis.Engine.Web.StaticFiles
{
    public interface IStaticFileSettings
    {
        string WebRoot
        {
            get;
        }
    }
}