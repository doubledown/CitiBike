﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;

namespace CitiBike.Analysis.Engine.Web.StaticFiles
{
    public sealed class Html5Middleware
    {
        private readonly string _defaultPath;
        private readonly StaticFileMiddleware _staticFileMiddleware;

        public Html5Middleware(Func<IDictionary<string, object>, Task> next, string webRoot, string defaultFileName)
        {
            this._staticFileMiddleware = new StaticFileMiddleware(next, new StaticFileOptions { FileSystem = new PhysicalFileSystem(webRoot) });
            this._defaultPath = !string.IsNullOrEmpty(defaultFileName) ? "/" + defaultFileName : null;
        }

        public async Task Invoke(IDictionary<string, object> environment)
        {
            IOwinRequest request = new OwinRequest(environment);
            if (this._defaultPath != null && request.Path.HasValue)
            {
                if (request.Path.Value != "/")
                {
                    // Attempt to serve it as a static file.
                    await this._staticFileMiddleware.Invoke(environment);
                    IOwinResponse response = new OwinResponse(environment);
                    if (response.StatusCode != (int)HttpStatusCode.NotFound || request.Path.Value.EndsWith(".map", StringComparison.Ordinal))
                    {
                        // If it is not a 404 or for a .map file on a minimized js file, allow the client to receive the 404.
                        return;
                    }
                }
                // Set the path to /index.html and rerun the request via the staticFileMiddleware.
                request.Path = new PathString(this._defaultPath);
            }
            await this._staticFileMiddleware.Invoke(environment);
        }
    }

    internal static class Html5MiddlewareMethods
    {
        public static IAppBuilder UseHtml5(this IAppBuilder builder, string webRoot, string defaultFileName = "index.html")
        {
            return builder.Use<Html5Middleware>(webRoot, defaultFileName);
        }
    }
}