﻿using Owin;

namespace CitiBike.Analysis.Engine.Web.StaticFiles
{
    public sealed class OwinStartup : IOwinStartup
    {
        private readonly string _webRoot;

        public OwinStartup(IStaticFileSettings settings)
        {
            this._webRoot = settings.WebRoot;
        }

        void IOwinStartup.OnStartup(IAppBuilder app)
        {
            app.UseHtml5(this._webRoot);
        }
    }
}