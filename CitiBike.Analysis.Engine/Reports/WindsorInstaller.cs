﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using CitiBike.Analysis.Engine.Windsor;

namespace CitiBike.Analysis.Engine.Reports
{
    public sealed class WindsorInstaller : IWindsorInstaller
    {
        void IWindsorInstaller.Install(IWindsorContainer container, IConfigurationStore store)
        {
            container
                .Kernel.Resolver.AddSubResolver(new ServiceArraySubDependencyResolver(container.Kernel, typeof(IReport)));
        }
    }
}