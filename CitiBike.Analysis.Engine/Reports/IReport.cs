﻿namespace CitiBike.Analysis.Engine.Reports
{
    public interface IReport
    {
        string Name
        {
            get;
        }

        object GetData(int year, int month, int day);
    }
}