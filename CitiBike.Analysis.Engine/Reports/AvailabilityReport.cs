﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using CitiBike.Analysis.Engine.Data;

namespace CitiBike.Analysis.Engine.Reports
{
    public sealed class AvailabilityReport : IReport
    {
        private const string Query = @"DECLARE @offsets TABLE([Offset] int)
;WITH [Offsets] AS (SELECT @startOffset [Offset] UNION ALL SELECT [Offset] + 1 FROM [Offsets] WHERE [Offset] < @startOffset + 23)
INSERT @offsets SELECT [Offset] FROM [Offsets]
SELECT
	o.[Offset] - @startOffset,
	v.[StationId],
	v.[Bikes]
FROM
	@offsets o
	CROSS APPLY (
		SELECT
			r.[StationId],
			o.[Offset],
			COUNT(r.[BikeId]) [Bikes]
		FROM
			(SELECT
				b.[BikeId],
				b.[StationId],
				RANK() OVER (PARTITION BY b.[BikeId] ORDER BY b.[StopTimeOffset] DESC) [Rank]
			FROM
				[dbo].[BikePosition] b
			WHERE
				b.[StopTimeOffset] <= o.[Offset]) r
		WHERE
			r.[Rank] = 1
		GROUP BY
			r.[StationId]
	) v
ORDER BY
	1, 2";

        private readonly IConnectionFactory _connectionFactory;
        private readonly Task<IReadOnlyDictionary<int, StationInfo>> _stationDataTask;

        public AvailabilityReport(IConnectionFactory connectionFactory)
        {
            this._connectionFactory = connectionFactory;
            this._stationDataTask = Task.Factory.StartNew(this.LoadStationData);
        }

        public string Name => "Availability";

        object IReport.GetData(int year, int month, int day)
        {
            return this.GetData(year, month, day);
        }

        private ReportData GetData(int year, int month, int day)
        {
            int startOffset = new DateTime(year, month, day).GetOffset();
            using (SqlConnection connection = this._connectionFactory.CreateConnection(true))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = AvailabilityReport.Query;
                    command.Parameters.AddWithValue("@startOffset", startOffset);
                    command.CommandTimeout = 600;
                    List<Datum> data = new List<Datum>();
                    using (SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        while (dataReader.Read())
                        {
                            data.Add(new Datum(dataReader.GetInt32(0), dataReader.GetInt32(1), dataReader.GetInt32(2)));
                        }
                    }
                    return new ReportData(this._stationDataTask.Result.Values, data);
                }
            }
        }

        private IReadOnlyDictionary<int, StationInfo> LoadStationData()
        {
            using (SqlConnection connection = this._connectionFactory.CreateConnection())
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id],[Name],[Capacity] FROM [dbo].[Station]";
                    using (SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        Dictionary<int, StationInfo> dictionary = new Dictionary<int, StationInfo>();
                        while (dataReader.Read())
                        {
                            int id = dataReader.GetInt32(0);
                            dictionary.Add(id, new StationInfo(id, dataReader.GetString(1), dataReader.GetInt32(2)));
                        }
                        return dictionary;
                    }
                }
            }
        }

        private struct Datum
        {
            public Datum(int hour, int stationId, int bikes)
            {
                this.Hour = hour;
                this.StationId = stationId;
                this.Bikes = bikes;
            }

            public int Bikes
            {
                get;
            }

            public int Hour
            {
                get;
            }

            public int StationId
            {
                get;
            }
        }

        private struct ReportData
        {
            public ReportData(IEnumerable<StationInfo> stations, IEnumerable<Datum> data)
            {
                this.Stations = stations;
                this.Data = data;
            }

            public IEnumerable<Datum> Data
            {
                get;
            }

            public IEnumerable<StationInfo> Stations
            {
                get;
            }
        }

        private struct StationInfo
        {
            public StationInfo(int id, string name, int capacity)
            {
                this.Id = id;
                this.Name = name;
                this.Capacity = capacity;
            }

            public int Capacity
            {
                get;
            }

            public int Id
            {
                get;
            }

            public string Name
            {
                get;
            }
        }

        private struct StationStatus : IComparable<StationStatus>
        {
            public StationStatus(int offset, int stationId, int bikes)
            {
                this.Offset = offset;
                this.StationId = stationId;
                this.Bikes = bikes;
            }

            public int Bikes
            {
                get;
            }

            public int Offset
            {
                get;
            }

            public int StationId
            {
                get;
            }

            public int CompareTo(StationStatus other)
            {
                return this.Offset.CompareTo(other.Offset);
            }
        }
    }
}