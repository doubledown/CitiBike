﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CitiBike.Analysis.Engine.Data;

namespace CitiBike.Analysis.Engine.Reports
{
    public sealed class WeatherReport : IReport
    {
        private const string Query = @"SELECT
	v.[DateId],
	v.[Rides],
    v.[Miles],
	ISNULL(iwd.[WeatherType], 0)
FROM
	(SELECT
		@monthId * 100 + DATEPART(DAY, b.[Date]) [DateId],
        SUM(b.[Rides]) [Rides],
        SUM(b.[Miles]) [Miles]
	FROM
		[dbo].[BikePosition] b
	WHERE
		b.[MonthId] = @monthId
	GROUP BY
		@monthId * 100 + DATEPART(DAY, b.[Date])) v
	LEFT JOIN [dbo].[InclementWeatherDay] iwd ON v.[DateId] = iwd.[DateId]
ORDER BY
	v.[DateId]";

        private readonly IConnectionFactory _connectionFactory;

        public WeatherReport(IConnectionFactory connectionFactory)
        {
            this._connectionFactory = connectionFactory;
        }

        public string Name => "Weather";

        object IReport.GetData(int year, int month, int day)
        {
            return this.GetData(year, month);
        }

        private ReportData GetData(int year, int month)
        {
            List<Datum> data = new List<Datum>();
            int[] ridesByWeather = new int[3]; // There are three weather types.
            int[] daysWithWeather = new int[3];
            double[] milesByWeather = new double[3];
            int totalRides = 0;
            int totalDays = 0;
            double totalMiles = 0;
            using (SqlConnection connection = this._connectionFactory.CreateConnection())
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = WeatherReport.Query;
                    command.Parameters.AddWithValue("@monthId", year * 100 + month);
                    command.CommandTimeout = 600;
                    using (SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        while (dataReader.Read())
                        {
                            int dateId = dataReader.GetInt32(0);
                            int rides = dataReader.GetInt32(1);
                            double miles = dataReader.GetDouble(2);
                            int weatherType = dataReader.GetInt32(3);
                            data.Add(new Datum(dateId, rides, miles, (WeatherType)weatherType));
                            totalMiles += miles;
                            totalRides += rides;
                            ridesByWeather[weatherType] += rides;
                            milesByWeather[weatherType] += miles;
                            daysWithWeather[weatherType]++;
                            totalDays++;
                        }
                    }
                }
            }
            return new ReportData(data)
            {
                TotalRides = totalRides,
                TotalMiles = totalMiles,
                MonthName = new DateTime(year, month, 1).ToString("MMMM"),
                AverageRidesPerDay = totalDays != 0 ? (double)totalRides / totalDays : 0d,
                AverageMilesPerDay = totalDays != 0 ? totalMiles / totalDays : 0d,
                SnowDays = daysWithWeather[(int)WeatherType.Snow],
                SnowDayRides = ridesByWeather[(int)WeatherType.Snow],
                SnowDayMiles = milesByWeather[(int)WeatherType.Snow],
                RainDays = daysWithWeather[(int)WeatherType.Rain],
                RainDayRides = ridesByWeather[(int)WeatherType.Rain],
                RainDayMiles = milesByWeather[(int)WeatherType.Rain],
                NormalDays = daysWithWeather[0],
                NormalDayRides = ridesByWeather[0],
                NormalDayMiles = milesByWeather[0]
            };
        }

        private struct Datum
        {
            public Datum(int dateId, int rides, double miles, WeatherType weatherType)
            {
                this.DateId = dateId;
                this.Rides = rides;
                this.Miles = miles;
                this.WeatherType = weatherType;
            }

            public int DateId
            {
                get;
            }

            public double Miles
            {
                get;
            }

            public int Rides
            {
                get;
            }

            public WeatherType WeatherType
            {
                get;
            }
        }

        private sealed class ReportData
        {
            public ReportData(IReadOnlyList<Datum> data)
            {
                this.Data = data;
            }

            public double AverageMilesPerDay
            {
                get;
                set;
            }

            public double AverageRidesPerDay
            {
                get;
                set;
            }

            public IReadOnlyList<Datum> Data
            {
                get;
            }

            public string MonthName
            {
                get;
                set;
            }

            public double NormalDayMiles
            {
                get;
                set;
            }

            public int NormalDayRides
            {
                get;
                set;
            }

            public int NormalDays
            {
                get;
                set;
            }

            public double RainDayMiles
            {
                get;
                set;
            }

            public int RainDayRides
            {
                get;
                set;
            }

            public int RainDays
            {
                get;
                set;
            }

            public double SnowDayMiles
            {
                get;
                set;
            }

            public int SnowDayRides
            {
                get;
                set;
            }

            public int SnowDays
            {
                get;
                set;
            }

            public double TotalMiles
            {
                get;
                set;
            }

            public int TotalRides
            {
                get;
                set;
            }
        }
    }
}