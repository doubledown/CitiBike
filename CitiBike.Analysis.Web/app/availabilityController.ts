﻿namespace CitiBike {
    interface IStationInfo {
        id: number;
        name: string;
        capacity: number;
    }

    interface IDatum {
        hour: number;
        bikes: number;
        stationId: number;
    }

    interface IResult {
        stations: IStationInfo[];
        data: IDatum[];
    }

    class AvailabilityController extends ReportController<IResult> {
        static $inject = ['$scope', '$http'];

        days = [] as number[];
        day = 1;
        hour = 0;
        stationData: IDatum[];

        private stations: { [id: number]: IStationInfo };
        private unsubscribeDay: () => void;
        private unsubscribeHour: () => void;

        constructor($scope: ng.IScope, $http: ng.IHttpService) {
            super($scope, $http);
        }

        get hourDisplay() {
            if (this.hour % 12 == 0) {
                return `12 ${this.hour == 0 ? 'A' : 'P'}M`;
            }
            const hour = this.hour % 12;
            const prefix = hour < 10 ? `0${hour}` : hour;
            return prefix + (this.hour < 12 ? ' AM' : ' PM');
        }

        getStationCapacity(stationId: number): number | string {
            let station: IStationInfo;
            return this.stations && (station = this.stations[stationId]).capacity ? station.capacity : 'Unknown';
        }

        getStationName(stationId: number) {
            return this.stations ? this.stations[stationId].name : null;
        }

        protected getDay() {
            return this.day;
        }

        protected getReportName() {
            return 'Availability';
        }

        protected onDataLoaded(data: IResult) {
            super.onDataLoaded(data);
            this.stations = {};
            _.forEach(data.stations, station => this.stations[station.id] = station);
            this.hour = 0;
            this.unsubscribeHour = this.$scope.$watch(() => this.hour, () => this.onHourChanged(), true);
        }

        protected onMonthChanged() {
            if (this.unsubscribeDay) {
                this.unsubscribeDay();
            }
            const daysInMonth = this.daysInMonth;
            if (this.days.length != daysInMonth) {
                this.days.splice(0, this.days.length);
                for (let number = 0; number < daysInMonth; number++) {
                    this.days.push(number + 1);
                }
            }
            this.day = 1;
            this.unsubscribeDay = this.$scope.$watch(() => this.day, () => this.onDayChanged(), true);
        }

        private onDayChanged() {
            if (this.unsubscribeHour) {
                this.unsubscribeHour();
            }
            this.hour = 0;
            this.loadData();
        }

        private onHourChanged() {
            if (!this.data) {
                return;
            }
            this.stationData = _.orderBy(
                _.filter(this.data.data, datum => datum.hour == this.hour && datum.stationId in this.stations),
                (datum: IDatum) => this.getStationName(datum.stationId)
            );
        }
    }

    app.controller('availabilityController', AvailabilityController);
}