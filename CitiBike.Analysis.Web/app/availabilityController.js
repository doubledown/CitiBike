var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var CitiBike;
(function (CitiBike) {
    var AvailabilityController = (function (_super) {
        __extends(AvailabilityController, _super);
        function AvailabilityController($scope, $http) {
            _super.call(this, $scope, $http);
            this.days = [];
            this.day = 1;
            this.hour = 0;
        }
        Object.defineProperty(AvailabilityController.prototype, "hourDisplay", {
            get: function () {
                if (this.hour % 12 == 0) {
                    return "12 " + (this.hour == 0 ? 'A' : 'P') + "M";
                }
                var hour = this.hour % 12;
                var prefix = hour < 10 ? "0" + hour : hour;
                return prefix + (this.hour < 12 ? ' AM' : ' PM');
            },
            enumerable: true,
            configurable: true
        });
        AvailabilityController.prototype.getStationCapacity = function (stationId) {
            var station;
            return this.stations && (station = this.stations[stationId]).capacity ? station.capacity : 'Unknown';
        };
        AvailabilityController.prototype.getStationName = function (stationId) {
            return this.stations ? this.stations[stationId].name : null;
        };
        AvailabilityController.prototype.getDay = function () {
            return this.day;
        };
        AvailabilityController.prototype.getReportName = function () {
            return 'Availability';
        };
        AvailabilityController.prototype.onDataLoaded = function (data) {
            var _this = this;
            _super.prototype.onDataLoaded.call(this, data);
            this.stations = {};
            _.forEach(data.stations, function (station) { return _this.stations[station.id] = station; });
            this.hour = 0;
            this.unsubscribeHour = this.$scope.$watch(function () { return _this.hour; }, function () { return _this.onHourChanged(); }, true);
        };
        AvailabilityController.prototype.onMonthChanged = function () {
            var _this = this;
            if (this.unsubscribeDay) {
                this.unsubscribeDay();
            }
            var daysInMonth = this.daysInMonth;
            if (this.days.length != daysInMonth) {
                this.days.splice(0, this.days.length);
                for (var number = 0; number < daysInMonth; number++) {
                    this.days.push(number + 1);
                }
            }
            this.day = 1;
            this.unsubscribeDay = this.$scope.$watch(function () { return _this.day; }, function () { return _this.onDayChanged(); }, true);
        };
        AvailabilityController.prototype.onDayChanged = function () {
            if (this.unsubscribeHour) {
                this.unsubscribeHour();
            }
            this.hour = 0;
            this.loadData();
        };
        AvailabilityController.prototype.onHourChanged = function () {
            var _this = this;
            if (!this.data) {
                return;
            }
            this.stationData = _.orderBy(_.filter(this.data.data, function (datum) { return datum.hour == _this.hour && datum.stationId in _this.stations; }), function (datum) { return _this.getStationName(datum.stationId); });
        };
        AvailabilityController.$inject = ['$scope', '$http'];
        return AvailabilityController;
    }(CitiBike.ReportController));
    CitiBike.app.controller('availabilityController', AvailabilityController);
})(CitiBike || (CitiBike = {}));
//# sourceMappingURL=availabilityController.js.map