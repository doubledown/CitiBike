var CitiBike;
(function (CitiBike) {
    var ReportController = (function () {
        function ReportController($scope, $http) {
            var _this = this;
            this.$scope = $scope;
            this.$http = $http;
            this.isLoading = false;
            this.month = 1;
            this.data = {};
            $scope.$watch(function () { return _this.month; }, function () { return _this.onMonthChanged(); }, true);
        }
        Object.defineProperty(ReportController.prototype, "daysInMonth", {
            get: function () {
                return new Date(2015, this.month, 0).getDate();
            },
            enumerable: true,
            configurable: true
        });
        ReportController.prototype.getDayOfWeek = function (day) {
            var date = new Date(2015, this.month - 1, day);
            switch (date.getDay()) {
                case 0:
                    return 'Sunday';
                case 1:
                    return 'Monday';
                case 2:
                    return 'Tuesday';
                case 3:
                    return 'Wednesday';
                case 4:
                    return 'Thursday';
                case 5:
                    return 'Friday';
            }
            return 'Saturday';
        };
        ReportController.prototype.getDay = function () {
            return 0;
        };
        ReportController.prototype.onDataLoaded = function (data) {
            this.data = data;
        };
        ReportController.prototype.onError = function (error) {
            console.log(error);
        };
        ReportController.prototype.onMonthChanged = function () {
            this.loadData();
        };
        ReportController.prototype.loadData = function () {
            var _this = this;
            this.isLoading = true;
            this.$http.get("api/report?name=" + this.getReportName() + "&year=2015&month=" + this.month + "&day=" + this.getDay())
                .then(function (response) { return _this.onDataLoaded(response.data); }, function (error) { return _this.onError(error); })
                .finally(function () { return _this.isLoading = false; });
        };
        return ReportController;
    }());
    CitiBike.ReportController = ReportController;
})(CitiBike || (CitiBike = {}));
//# sourceMappingURL=reportController.js.map