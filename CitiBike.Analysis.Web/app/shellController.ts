﻿namespace CitiBike {
    class ShellController {
        static $inject = ['$scope', '$location', '$mdSidenav'];

        menu: ng.material.ISidenavObject;
        private view = 'Weather';

        constructor($scope: ng.IScope, private $location: ng.ILocationService, $mdSidenav: ng.material.ISidenavService) {
            $mdSidenav('left', true).then(menu => this.menu = menu);
            $scope.$watch(() => $location.path(), newValue => this.onPathChanged(newValue), true);
        }

        private onPathChanged(path: string) {
            const startsWith = (text: string) => path.length >= text.length && path.substring(0, text.length) == text;

            if (path == '/' || startsWith('/weather')) {
                this.view = 'Weather';
            }
            else if (startsWith('/availability')) {
                this.view = 'Availability';
            }
        }

        get currentView() {
            return this.view;
        }

        set currentView(view: string) {
            this.menu.close();
            if (this.view == view) {
                return;
            }
            let newPath: string;
            switch (view) {
                case 'Weather':
                    newPath = '/';
                    break;
                case 'Availability':
                default:
                    newPath = '/availability';
                    break;
            }
            this.$location.path(newPath);
        }
    }

    app.controller('shellController', ShellController);
}