﻿namespace CitiBike {
    export abstract class ReportController<TData> {
        isLoading = false;
        month = 1;
        data = {} as TData;

        constructor(protected $scope: ng.IScope, protected $http: ng.IHttpService) {
            $scope.$watch(() => this.month, () => this.onMonthChanged(), true);
        }

        get daysInMonth() {
            return new Date(2015, this.month, 0).getDate();
        }

        getDayOfWeek(day: number) {
            const date = new Date(2015, this.month - 1, day);
            
            switch (date.getDay()) {
                case 0:
                    return 'Sunday';
                case 1:
                    return 'Monday';
                case 2:
                    return 'Tuesday';
                case 3:
                    return 'Wednesday';
                case 4:
                    return 'Thursday';
                case 5:
                    return 'Friday';
            }
            return 'Saturday';
        }

        protected getDay() {
            return 0;
        }

        protected abstract getReportName(): string;

        protected onDataLoaded(data : TData) {
            this.data = data;
        }

        protected onError(error) {
            console.log(error);
        }

        protected onMonthChanged() {
            this.loadData();
        }

        protected loadData() {
            this.isLoading = true;
            this.$http.get<TData>(`api/report?name=${this.getReportName()}&year=2015&month=${this.month}&day=${this.getDay()}`)
                .then(response => this.onDataLoaded(response.data), error => this.onError(error))
                .finally(() => this.isLoading = false);
        }
    }
}