var CitiBike;
(function (CitiBike) {
    var MonthSelectorController = (function () {
        function MonthSelectorController($scope) {
            var _this = this;
            this.$scope = $scope;
            this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            this.monthIndex = 0;
            $scope.$watch(function () { return _this.monthIndex; }, function (newValue) { return $scope.month = parseInt(newValue) + 1; }, true);
        }
        MonthSelectorController.$inject = ['$scope'];
        return MonthSelectorController;
    }());
    CitiBike.app.directive('monthSelector', function () { return {
        restrict: 'E',
        templateUrl: 'app/monthSelector.html',
        scope: {
            month: '=',
            year: '@',
            loading: '='
        },
        controller: MonthSelectorController,
        controllerAs: 'mc',
        transclude: true
    }; });
})(CitiBike || (CitiBike = {}));
//# sourceMappingURL=monthSelector.js.map