﻿namespace CitiBike {
    interface IMonthSelectorScope extends ng.IScope {
        month: number;
        year: number;
    }

    class MonthSelectorController {
        static $inject = ['$scope'];

        readonly monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        monthIndex = 0;

        constructor(private $scope: IMonthSelectorScope) {
            $scope.$watch(() => this.monthIndex, (newValue: any) => $scope.month = parseInt(newValue) + 1, true);
        }
    }

    app.directive('monthSelector', () => <ng.IDirective>{
        restrict: 'E',
        templateUrl: 'app/monthSelector.html',
        scope: {
            month: '=',
            year: '@',
            loading: '='
        },
        controller: MonthSelectorController,
        controllerAs: 'mc',
        transclude: true
    });
}