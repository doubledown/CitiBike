var CitiBike;
(function (CitiBike) {
    var ShellController = (function () {
        function ShellController($scope, $location, $mdSidenav) {
            var _this = this;
            this.$location = $location;
            this.view = 'Weather';
            $mdSidenav('left', true).then(function (menu) { return _this.menu = menu; });
            $scope.$watch(function () { return $location.path(); }, function (newValue) { return _this.onPathChanged(newValue); }, true);
        }
        ShellController.prototype.onPathChanged = function (path) {
            var startsWith = function (text) { return path.length >= text.length && path.substring(0, text.length) == text; };
            if (path == '/' || startsWith('/weather')) {
                this.view = 'Weather';
            }
            else if (startsWith('/availability')) {
                this.view = 'Availability';
            }
        };
        Object.defineProperty(ShellController.prototype, "currentView", {
            get: function () {
                return this.view;
            },
            set: function (view) {
                this.menu.close();
                if (this.view == view) {
                    return;
                }
                var newPath;
                switch (view) {
                    case 'Weather':
                        newPath = '/';
                        break;
                    case 'Availability':
                    default:
                        newPath = '/availability';
                        break;
                }
                this.$location.path(newPath);
            },
            enumerable: true,
            configurable: true
        });
        ShellController.$inject = ['$scope', '$location', '$mdSidenav'];
        return ShellController;
    }());
    CitiBike.app.controller('shellController', ShellController);
})(CitiBike || (CitiBike = {}));
//# sourceMappingURL=shellController.js.map