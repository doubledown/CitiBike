﻿namespace CitiBike {
    class WeatherController extends ReportController<any> {
        static $inject = ['$scope', '$http'];

        constructor($scope: ng.IScope, $http: ng.IHttpService) {
            super($scope, $http);
        }

        protected getReportName() {
            return 'Weather';
        }
    }

    app.controller('weatherController', WeatherController);
}