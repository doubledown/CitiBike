﻿namespace CitiBike {
    class Configuration {
        static $inject = ['$locationProvider', '$routeProvider'];

        constructor($locationProvider: ng.ILocationProvider, $routeProvider: ng.route.IRouteProvider) {
            $routeProvider
                .when('/availability', {
                    templateUrl: 'app/availability.html',
                    controller: 'availabilityController',
                    reloadOnSearch: false,
                    caseInsensitiveMatch: true,
                    controllerAs: 'ctrl'
                })
                .when('/', {
                    templateUrl: 'app/weather.html',
                    controller: 'weatherController',
                    reloadOnSearch: false,
                    caseInsensitiveMatch: true,
                    controllerAs: 'ctrl'
                });
            $locationProvider.html5Mode(true);
        }
    }

    app.config(Configuration);
}