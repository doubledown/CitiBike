var CitiBike;
(function (CitiBike) {
    var Configuration = (function () {
        function Configuration($locationProvider, $routeProvider) {
            $routeProvider
                .when('/availability', {
                templateUrl: 'app/availability.html',
                controller: 'availabilityController',
                reloadOnSearch: false,
                caseInsensitiveMatch: true,
                controllerAs: 'ctrl'
            })
                .when('/', {
                templateUrl: 'app/weather.html',
                controller: 'weatherController',
                reloadOnSearch: false,
                caseInsensitiveMatch: true,
                controllerAs: 'ctrl'
            });
            $locationProvider.html5Mode(true);
        }
        Configuration.$inject = ['$locationProvider', '$routeProvider'];
        return Configuration;
    }());
    CitiBike.app.config(Configuration);
})(CitiBike || (CitiBike = {}));
//# sourceMappingURL=configuration.js.map