var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var CitiBike;
(function (CitiBike) {
    var WeatherController = (function (_super) {
        __extends(WeatherController, _super);
        function WeatherController($scope, $http) {
            _super.call(this, $scope, $http);
        }
        WeatherController.prototype.getReportName = function () {
            return 'Weather';
        };
        WeatherController.$inject = ['$scope', '$http'];
        return WeatherController;
    }(CitiBike.ReportController));
    CitiBike.app.controller('weatherController', WeatherController);
})(CitiBike || (CitiBike = {}));
//# sourceMappingURL=weatherController.js.map